DEVOPS

# WEEK 5
## VirtualBox vs VMware 

O VirtualBox e o VMware são sistemas de virtualização, ou seja, ambos são softwares
que podem executar simultanemente várias máquinas num servidor físico. <br> <br>

O papel do Vagrant é gerir o ambiente das máquinas virtuais. 

<h3> VirtualBox </h3>

O VirtualBox é executado no Windows, Linux, Mac OS e 
Solaris e suporta uma ampla variedade de sistemas operacionais 
convidados, do Windows ao Linux, Mac, Solaris, OpenSolaris, OS/2 e 
variações do BSD.


<h3>VMware</h3>
O VMware tem versões para Linux e Windows, sendo que para Mac OS deve ser utilizado o VMware Fusion.
Note-se que existem diferentes versões: VMware ESXi, VMware Player, VMware Workstation e VMware Fusion.

<h3> DIFERENÇAS </h3>

<h4> Interface de usuário </h4>

Ambas a plataformas oferecem uma ampla variedade de interfaces.


<h4> USB </h4>

Ambas a plataformas suportam a conexão de dispositivos USB a máquina virtuais.
Enquanto os usuários da VMware oferecem suporte a dispositivos USB prontos para uso, os usuários da VB precisam de usar o Extension Pack para habilitar o suporte
a USB 2.0 e 3.0. 

<h4> Pastas partilhadas </h4>
É possível partilhar pastas em ambos os virtualizadores, exceto na versão
VMware ESXi, que requer a criaçõa das pastas ao nível do sistema operacional do host.

<h4> Formato do disco virtual </h4>
Aqui as diferenças são maiores. O único disco compatível na VMware é o VMDK, enquanto na VB existe um maior
leque de escolhas: formato VDI nativo, VHD, HDD e VMDK.

<h4> Redes Virtuais </h4>

**Virtual Box:** <br>
* Não anexado
* Tradução de endereço de rede (NA)
* Serviço de rede NAT
* Adaptador em point
* Rede interna
* Adaptador apenas de host <br> <br>

**VMware:**
* Com ponte
* NAT
* Host only

NOTA: Não instalar o VMware Fusion no meu computador, não tendo sido permitido o processo de continuação de instalação.



