DEVOPS

# WEEK 1
## Master branch exercise



Em primeiro lugar, é importante referir que estou a trabalhar no sistema operativo iOS. <br>
Tendo em conta que tive alguns precalços durante o desenvolvimento do exercício, o número de commits é maior do que o esperado.<br>
A elaboração de testes, na qual será esperado o uso de alguns dos comandos referidos no tema 1.2, será feita até feita até à próxima semana. Deixo esta informação, uma vez que, dados os imprevistos, foi priorizado o conhecimento dos comandos Git. 


>**GIT SETUP:**

Configuração do git <br>
    **git config --global** <br> <br>
    Configuração do git através do output **git config --global**  <br> <br>
    Anexado a esse output foi identificado o user através dos outputs: <br> <br>
    **git config --global user.name ‘Ana Guedes’** <br> <br>
    **git config --global user.email ‘1211748@isep.ipp.pt’** <br> <br>
    **git config –-global core.editor nano**
    <br>

### 1. COMO COPIAR O CONTEÚDO DE UMA PASTA PARA OUTRA?
Para copiar o código de Tutorial React.js and Spring Data REST Application para uma nova pasta chamada CA1 utilizei
o seguinte endereço: <br> <br> **cp -r tut-react-and-spring-data-rest/CA1** <br> <br>

### 1.1 COMO SE COPIOU O CÓDIGO ATRAVÉS DO TERMINAL?
Para chegar a este caminho, foi primeiramente necessário identificar a pasta. <br> <br>
**Passo 1)** **cd tab**; <br> <br>
**Passo 2)** Visualizando todas as pastas e, sabendo que pretendo seguir a pasta Documents, seguido de cd escrevi Doc tab que, automaticamente, remeteu-me para o endereço **cd Documents/** <br> <br>
**Passo 3)** Para ver os documentos presentes na pasta Documents, utilizei o comando **ls**, que tem por objetivo listar todos os documentos disponíveis nessa pasta. <br> <br>
**Passo 4)** Selecionei o ficheiro DEVOPS através de cd DEVOPS. Entrando nessa pasta, encontrei a pasta final (devops-21-22-lmn-1211748), a qual acedi. <br> <br>
**Passo 5)** Para criar a pasta CA1 (nome sugerido pelo exercício), foi usado o comando **mkdir CA1**. Nessa pasta, foi criado o ficheiro **README.md** através do output **touch README.md**  <br> <br>
**Passo 6)** Copiei o conteúdo presente no diretório tut-react-and-spring  para o diretório CA1, utilizando o output **cp -r tut-react-and-spring-data-rest/CA1**

### 1.2 COMO ENVIAR AS ALTERAÇÕES PARA O REPOSITÓRIO REMOTO?
Para que todos os ficheiros fiquem alocados no repositório remoto utilizei os seguintes outputs <br> <br>
1. **git add .** <br> <br>
2. **git commit -m 'new initial commit’** <br> <br>
3. **git push**

**Nota**: Quando surgem erros de escrita surge o output **dquote>**,
sendo necessário inserir o comando **'** (Exit) para voltar ao terminal. 

### 1.3 COMO ANEXAR TAGS A COMMITS?
Para inserir uma tag usei os seguintes outputs: <br> <br>
**1. git tag -a v11 fad9cac -m ‘tentativa’** <br> <br>
**2. git push origin v11** <br> <br>
Nota: fad9cac corresponde ao commit no qual se pretende acrescentar a tag 'tentativa'


Nota: Tendo em conta que me esqueci do nome dado à mensagem associada à tag, utilizei o output **git show ca1-part1** que me deu essa informação. Ao contrário das tags realizadas até então, esta tag não está listada nos commits. Estando associada ao repositório, esta encontra-se nessa mesma página junto à caixa de seleção "master". Clicando nessa caixa, é possível selecionar as tags, pelo que selecionando **ca1-part1** compreende-se que esta se refere ao repositório. 



# WEEK 2
## Master branch exercise

### 2.1 COMO SE CRIA UM BRANCH?

Em primeiro lugar, o git reconhece o branch em que o utilizador se encontra através do comando **git branch** <br> 
Para mudar de branch usa-se o comando **git checkout email-field** (Exemplo com o branch email-field)<br>

Para a criação de um branch email-field, foram utilizados os seguintes outputs: <br> <br>

**git branch email-field** <br> <br>
**git commit -a -m 'added a branch'** <br> <br>
**git push origin email-field** <br> <br>

Inicialmente, adicionei o branch emai-field ao diretório **devops-ANA-1211748** e não ao repositório **devops-21-22-lmn-1211748**, pelo que para apagá-lo e corrigir o erro usaram-se os seguintes outputs: <br> <br>
1. **git checkout master** <br> <br>
2. **git merge email-field** Nota: Para apagar o branch email-field é necessário transferi-lo para o master. Só assim é possível eliminá-lo.<br><br>
3. **git branch -d email-field**<br> <br>

Nota: Para apagar tags é necessário utilizar o seguinte output: <br><br>
**git push origin --delete v.ana** (Exemplo com a tag **v.ana**) <br>

Comentário: Ao longo do exercício, não estava a compreender o conceito **origin** associado ao push. No entanto, compreendi que o **git push** assume a existência de um repositório remoto definido por um branch que, por defeito, é o **origin**. Quando criamos outro branch e indicamos **git push origin** estamos a indicar que o trabalho está a ser executado sobre esse branch. Neste caso, **email-field**
Questionei-me sobre o tema quando quis apagar a tag **v.ana**, que exigiu um **origin** no output. Sem esse **origin** era assumido o branch **master** <br> <br>


