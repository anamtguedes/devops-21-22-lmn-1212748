DEVOPS

# WEEK 5
## Virtualization with Vagrant - Part 2

O Vagrant é um ficheiro de texto que permite descrever infrastuturas como, por exemplo, máquinas virtuais.
O ficheiro Vagrant permite recriar o ambiente virtual em qualquer contexto, sendo baseado na linguagem ruby.
<br> <br>
>As três componentes do Vagrant: <br>
1.**Box** <br>
2.**Provider** <br>
3.**Vagrantfile** <br>


<h3> COMANDOS VAGRANT </h3>

>**box**            manages boxes: installation, removal, etc. <br> <br>
**cloud**           manages everything related to Vagrant Cloud <br> <br>
**destroy**         stops and deletes all traces of the vagrant machine <br> <br>
**global-status**   outputs status Vagrant environments for this user <br> <br>
**halt**            stops the vagrant machine <br> <br>
**help**            shows the help for a subcommand <br> <br>
**hostmanager**    plugin: vagrant-hostmanager: manages the /etc/hosts file within a multi-machine environment <br> <br>
**init**            initializes a new Vagrant environment by creating a Vagrantfile <br><br>
**login**
**package**        packages a running vagrant environment into a box<br><br>
**plugin**          manages plugins: install, uninstall, update, etc.<br><br>
**port**            displays information about guest port mappings<br><br>
**powershell**      connects to machine via powershell remoting<br><br>
**provision**       provisions the vagrant machine<br><br>
**push**         deploys code in this environment to a configured destination<br><br>
**rdp**             connects to machine via RDP<br><br>
**reload**          restarts vagrant machine, loads new Vagrantfile configuration<br><br>
**resume**          resume a suspended vagrant machine<br><br>
**snapshot**        manages snapshots: saving, restoring, etc.<br><br>
**ssh**            connects to machine via SSH<br><br>
**ssh-config**      outputs OpenSSH valid configuration to connect to the machine<br><br>
**status**          outputs status of the vagrant machine<br><br>
**suspend**        suspends the machine<br><br>
**up**              starts and provisions the vagrant environment<br><br>
**upload**          upload to machine via communicator<br><br>
**validate**       validates the Vagrantfile<br><br>
**version**        prints current and latest Vagrant version<br><br>
**winrm**           executes commands on a machine via WinRM<br><br>
**winrm-config**    outputs WinRM configuration to connect to the machine.<br><br>

<h3> INSTALAÇÃO DA PRIMEIRA BOX </h3>

> config.vm.box = "envimation/ubuntu-xenial"

1. Instalação do Vagrant (2.2.19) no sistema operativo. A confirmação da sua instalação foi feita através do 
comando **vagrant -v**. <br>

> Nota: Comando **pwd** permite saber em que diretório o utilizador está.

2. Criação de uma nova pasta através do comando **mkdir my-first-vagrant**. Posteriormente, cria-se, dentro desse diretório, uma box através do comando
**vagrant init envimation/ubuntu-xenial**. O que se segue a seguir a **init** identifica o tipo de máquina virtual pretendida. <br> <br>
Ao colocar o comando suprarrreferido no terminal é criado um **Vagrantfile** na pasta **my-first-vagrant**,
aplicando-se o **vagrant up** para executar o ambiente virtual. O vagrant up procura o **Vagrantfile** e cria o ambiente 
virtual definido pela box **envimation/ubuntu-xenial**. <br> 

Ao abrirmos a VM, constatamos que a única máquina que está a correr é a **my-first-vagrant**. Isto aconteceu após a execução do 
comando **vagrant up**. 

> Nota: Se eu fizer **vagrant status**, é-me dito o qual o status do ambiente virtual na pasta. Simulando no meu 
terminal notifica que está a correr na virtual box.

3. Se eu quiser entrar na box, basta escrever o comando **vagrant ssh**, não sendo 
necessário escrever username nem password.

<h3> LIGAR E DESLIGAR A BOX ATRAVÉS DO TERMINAL </h3>

1. Parar a máquina virtual: comando **vagrant halt**.
2. Ligar a máquina virtual: comando **vagrant up**
3. Entrar na box: comando **vagrant ssh**

>NOTA: Sempre que são feitas alterações no Vagrantfile é necessário desligar e ligar a máquina ou utilizar 
o comando **vagrant reload**.

<h3> INSTALAÇÃO DA SEGUNDA BOX </h3>

>  config.vm.box = "bento/ubuntu-16.04"

Através da sequência de comandos **vagrant halt** e **vagrant up**, surge na 
máquina virtual uma pasta chamada **/vagrant** que irá conter tudo aquilo que 
está na pasta atual.

1. Entrar na máquina virtual: **vagrant ssh**, na qual está a pasta chamada **/vagrant**, cujo conteúdo é o da pasta corrente.
Para entrar dentro desta pasta, recorre-se ao comando **cd /vagrant/**. <br> <br>

2. Criação de um ficheiro vazio através do comando **touch teste.txt**. Abrindo um terminal pelo host, 
verifica-se que o ficheiro referido está lá criado. Para simular a escrita nesse documento, recorre-se
ao comando **nano teste.txt**. Voltando ao terminal ligado à máquina virtual, verifica-se que o texto 
está tambémm nela guardado pelo comando **cat teste.txt**. <br> <br>

3. Criação de um segundo ficheiro **mkdir my-second-vagrant** com uma nova box: **bento/ubuntu-16.04**. O ficheiro associado é igualmente transferido
   em formato **Vagrantfile**. <br> <br>
a. Correr ficheiro - **vagrant status**. <br>
b. Entrar na máquina virtual - **vagrant ssh** <br>
c. Para a máquina virtual - **vagrant halt**

> É possível encontrar as box disponíveis no seguinte link: https://app.vagrantup.com/boxes/search. Pelo número de downolads,
constata-se que a box mais usado é o **ubuntu/trusty64**.

<h3> PROVISIONAMENTO DA MÁQUINA VIRTUAL </h3>

Fazer o aprovisionamento da máquina virtual trata-se de referir que tipo de software que queremos introduzir. 

<h3> VAGRANT-BASIC-EXAMPLE </h3>

1. Clonar o repositório - git@github.com:atb/vagrant-basic-example.git. Como não estava a funcionar, optei por
fazer o download e adicioná-lo ao meu diretório - **CA3-part2** -, entrar no ficheiro associado - **vagrant-basic-example** -, seguido do comnando
**vagrant up**. Durante o processo, aparece a verde a sinalização de **running provision shell**.

>**O QUE SIGNIFICA FAZER O PROVISIONAMENTO?** <br> O provisionamento abrange todas as operações necessárias para criar uma nova máquina e colocá-la em funcionamento, 
incluindo, por exemplo, a definição do hardware físico, instalação do software, definição de armazenamento. 
Para provisionar uma máquina, o Vagrant precisa de um virtualizador como o **VirtualBox**, 
**VMWare**, **Docker** e o **KVM**. <br>  

2. No Vagrantfile disponibilizado no repositório do Professor Alexandre -https://bitbucket.org/atb/tut-basic-gradle/src/master/ -, verificamos que o mapeamento passou da porta 80 para a porta 8010. 
Desta forma, ao colocar no browser **localhost:8010**, é encontrado o texto disponibilizado no ficheiro html do repositório: 
**This is an example paragraph. Anything in the body tag will appear on the page, just like this p tag and its contents.** <br> <br>
Se voltar a desligar e a ligar a máquina, através dos comandos **vagrant halt** e **vagrant up**, constata-se que o **provision** já está
instalado. <br>

> Para verificar a instalação do provision, basta abrir o ficheiro **Vagrantfile** para confirmar: <br> <br> 
> config.vm.provision "shell", inline: <<-SHELL <br>
apt-get update <br>
apt-get install -y apache2 <br>
apt-get install -y git <br> 

No entanto, caso haja alterações no Vagrantfile, faz-se o seguinte: <br> <br>
>1.**vagrant halt** <br>
2.**vagrant up --provision** (Este comando serve para ligar o computador com o provision atualizado). 
<br> 

Para limpar os ficheiros da VB, exceto a box descarregada:
1.**vagrant destroy -f**
<br> <br>

Para remover a box:
1. **vagrant box remove + nome da box**
<br> <br>

<h3> DUAS MÁQUINAS VIRTUAIS </h3>

Na segunda parte do exercício, pretende-se criar duas VM com um
único Vagrantfile: <br> <br>
VM 1 - base de dados (db) <br>
VM 2 - código java e react <br>


Abrindo o segundo repositório disponibilizado pelo Professor Alexandre: https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/, encontramos o ficheiro Vagrantfile,
com as seguintes indicações: <br> <br>

><h4> BOX USADA </h4> **config.vm.box = "envimation/ubuntu-xenial"**

><h4>SECÇÃO DE PROVISION</h4> São definidas as configurações para as duas box: <br> <br>
config.vm.provision "shell", inline: <<-SHELL <br>
sudo apt-get update -y <br>
sudo apt-get install iputils-ping -y <br>
sudo apt-get install -y avahi-daemon libnss-mdns <br>
sudo apt-get install -y unzip <br>
sudo apt-get install openjdk-11-jdk-headless -y <br>
SHELL

><h4>CONFIGURAÇÃO DA PRIMEIRA MÁQUINA VIRTUAL (db) </h4> 
config.vm.define "db" do |db|<br>
db.vm.box = "envimation/ubuntu-xenial" (box) <br>
db.vm.hostname = "db" (nome do computador) <br>
db.vm.network "private_network", ip: "192.168.56.11" (ip da máquina virtual) <br>

><h4>MAPEAMENTO DOS PORTOS </h4>
db.vm.network "forwarded_port", guest: 8082, host: 8082<br>
db.vm.network "forwarded_port", guest: 9092, host: 9092<br> <br>
NOTA: A base de dados continuará a ser a H2, mas na versão base o H2 está a ser usado em memória.
A partir de agora, o H2 será exectuado na VM referida, sendo necessário definir os portos.

><h4>SECÇÃO PROVISION </h4>
Nesta secção, é dito o que eu quero instalar na máquina do ubuntu, ou seja, o H2 (biblioteca do java). <br> <br>
**PRIMEIRO PROVISION** <br>
db.vm.provision "shell", inline: <<-SHELL <br>
wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar <br>
SHELL <br> <br>
**SEGUNDO PROVISION** <br> <br>
Ao contrário do outro provision, este provision irá executar sempre. <br> <br>
db.vm.provision "shell", :run => 'always', inline: <<-SHELL <br>
java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt & <br>
SHELL <br> <br>

><h4>CONFIGURAÇÃO DA SEGUNDA MÁQUINA VIRTUAL (web) </h4>
config.vm.define "web" do |web|<br>
web.vm.box = "envimation/ubuntu-xenial"<br>
web.vm.hostname = "web"<br>
web.vm.network "private_network", ip: "192.168.56.10"<br> <br>
<h3> MEMÓRIA USADA PARA A SEGUNDA VM </h3>
web.vm.provider "virtualbox" do |v| <br> 
v.memory = 1024 <br>
end <br>

><h4>MAPEAMENTO DOS PORTOS </h4>
web.vm.network "forwarded_port", guest: 8080, host: 8080 <br> <br>

><h4>SECÇÃO DE PROVISION</h4>
sudo apt-get install git -y <br>
sudo apt-get install nodejs -y <br>
sudo apt-get install npm -y <br>
sudo ln -s /usr/bin/nodejs /usr/bin/node <br>
sudo apt install tomcat8 (NOTA: o tomcat é um servidor web para java. A aplicação
será copiada para o tomcat)-y <br>
sudo apt install tomcat8-admin -y <br> <br>

><h4>ALTERANDO O DOC VAGRANTFILE DO REPOSITÓRIO DO PROF. ALEXANDRE COM O NOSSO REPOSITÓRIO </h4>
git clone https://anamtguedes@bitbucket.org/anamtguedes/devops-21-22-lmn-1212748.git <br>
cd tut-basic-gradle <br>
chmod u+x gradlew <br>
./gradlew clean build (compilar a aplicação) <br><br>
Posteriormente, é criado um ficheiro **.war** na pasta **/build/libs/**:
sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war (NOTA: Este ficheiro vai ser copiado para a pasta do tomcat, onde vai correr a aplicação)<br>
/var/lib/tomcat8/webapps <br>
SHELL <br>

1. Clonar o repositório -**git clone https://anamtguedes@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git** - para o terminal.
No final do processo, verifica-se que as duas máquinas virtuais estão a a correr. <br> <br>

2. Utilizando o URL -**http://localhost:8080/basic-0.0.1-SNAPSHOT/** ou **http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/** é possível ver o que está a acontecer na VM2 (web): <br>
   ![imagem](imagem.png)

3. Utilizando o URL -**http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console** -, é possível verificar a VM1 (db): <br>
   ![imagem2](imagem2.png) <br> 

4. Nesse URL, é ainda possível proceder a alterações, verificando-as, posteriormente, através da VM web. <br>
   <img alt="imagem3" height="150px" src="imagem3.png" width="400px"/> <br> <br>
   <img alt="imagem4" height="50px" src="imagem4.png" width="50px"/>









