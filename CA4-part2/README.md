DEVOPS

# WEEK 7
## Containers & Dockers 

<h4> ALGUNS COMANDOS DOCKER COMPOSE </h4>

> **docker-compose up**: cria e inicia os containers <br>
> **docker-compose build**: realiza apenas a etapa de build das imagens usadas <br>
> **docker-compose restart**: reinicia os containers <br>
> **docker-compose start**: inicia os containers <br>
> **docker-compose stop**: para os containers <br>
> **docker-compose down**: remove todos os containers e componentes, como rede, imagem e volume <br>


<h4> SOBRE A CONSTITUIÇÃO DO ARQUIVO YAML </h4>

    1.Service - o Docker trata todos os containers como serviços, sendo essa a forma que devemos referenciar no arquivo yaml 
    2.Image ou Build - o Docker sabe qual a imagem que está a correr 
    3.Ports - informa sobre a porta do host que receberá as requisições 
    4.Expose - portas abertas para receber as requisições 
    5.Networks - redes que deverão ser criadas para que os serviços façam parte
    6.Volume - diretórios externos conectados ao Docker

1. Adicionar o ficheiro **docker-compose.yml** à pasta CA4-part2 e 
copiar o ficheiro **docker-compose.yml** do repositório do Professor Alexandre - **https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/**. 

2. Nesse repositório alteraram-se as linhas 9, 21 e 27 para: <br>

**ipv4_address: 192.168.56.10** <br> <br>
**ipv4_address: 192.168.56.11** <br> <br>
**- subnet: 192.168.56.0/24** <br> <br>

3. O repositório acima referido, será igualmente necessário para extrair o Dockerfile DB (corre em ubuntu) e o Dockerfile WEB 
(corre em tomcat) <br>

>Nota: Qual a diferença entre um ficheiro docker-compose.yml e um ficheiro dockerfile? A diferença é que no primeiro é possível criar uma série de serviços e 
configurações, enquanto o segundo descreve a imagem/box. Desta forma, os dois são complementares.

Introduzindo o comando **docker-compose build**, verificou-se o seguinte problema:
![img.png](Imagens/Error.png)

Mostra informação sobre o espaço ocupado pelo docker: 
![img.png](Imagens/imagem.png)

Limpar as caches através do comando 
![img.png](Imagens/prune.png)

3. **docker-compose build** <br> <br> 

4. **docker-compose up** <br> <br>

5. Colocar no browser os seguintes links: <br> <br>


    http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ 
    http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

7. **docker login** <br> <br>

8. **docker tag 77b025759806 anaguedes/ca4-partbweb** (o ID é referente à web) <br> <br>

9. **docker push anaguedes/ca4-partbweb** <br> <br>

10. **docker tag 832266b24c26 anaguedes/ca4-partbdb** (o ID é referente à db) <br> <br>

<h4> ALTERNATIVA: KUBERNETES </h4>

O Docker e o Kubernetes são tecnologias que podem operar em simultâneo.
O Docker possui o motor Docker, permitindo compilar e executar contentores em qualquer máquina, armazenar e partilhar imagens
através de um registo de contentor, tal é o casao do Docker Hub. <br> <br>

À medida que as aplicações crescem e ocupam vários contentores em diversos servidores, a complexidade aumenta. É nesta situação que o Kubernetes pode ajudar,
permitindo executar os contentores do Docker e a ajudar a contornar algumas complexidadades operacionais. <br> <br>


A unidade básica do Kubernetes são os **pods**, correspondendo a um ou mais containers
Linux. No sistema do Kubernetes, os containers de um **pod** compartilham os mesmos recursos de computação. Esses recursos
são agrupados em **clusters**. <br> <br>

**CONCEITOS IMPORTANTES SOBRE O KUBERNETES** <br> <br>

<h3> HARDWARE </h3>

**Nó** - menor unidade de hardware na computação Kubernetes, sendo uma VM ou computador físico. Cada nó tem um Kubelet,
um agente que gerencia o no e comunica a camada de gerenciamento Kubernetes. O nó deve ter ferramentas para lidar
com operapões de container, como o Docker<br> 
**Cluster** - conjunto de nós agrupados para que os recursos sejam 
partilhados de modo inteligente. O cluster Kubernetes consiste em dois tipos de recursos: Camada de gerenciamento (Control Plane) e Nós (Nodes) <br>


<h3> SOFTWARE </h3>

**Container**: conjunto de um ou mais processos com todos os arquivos necessários para a execução. <br>
**Pod**: um conjunto de um ou mais containers Linux agrupados em um mesmo pacote para maximizar os benefícios do compartilhamento 
de recursos por meio do gerenciamento de clusters. <br>

    1.Cada máquina de hardware é representada por Kubernetes como um nó. 
    2.Os vários nós são agrupados em clusters
    3.Os pods são executados nos clusters, assegurando que os containers sejam executados no mesmo cluster

<h3> ALGUNS COMANDOS  KUBECTL </h3>

**kubectl get**- listar recursos <br>
**kubectl describe** - mostrar informações detalhadas sobre um recurso <br>
**kubectl exec** - executar um comando em um container num Pod <br>

![img.png](Imagens/cluster.png)
Source: https://azure.microsoft.com/pt-pt/topic/kubernetes-vs-docker/

![img.png](Imagens/nodes.png)
Source: https://azure.microsoft.com/pt-pt/topic/kubernetes-vs-docker/

<h3> INSTALAÇÃO </h3>

Ferramentas que permitem criar um cluster Kubernetes em ambiente virtual: <br> <br>
[1. Minikube](https://minikube.sigs.k8s.io/docs/start/) <br> <br>
[2. KinD ](https://kind.sigs.k8s.io/docs/user/quick-start/)
