DEVOPS

# WEEK 6
## Containers & Dockers 

<h3> PRIMEIRO CONTACTO COM O DOCKER </h3>

1. Verificar a versão do docker instalado no computador, através do terminal: **docker --version** <br> <br>
**Docker version 20.10.14** <br> <br>

2. Instalar o container **docker run --name my-ubuntu-container -i -t ubuntu /bin/bash**. <br> <br>
* O sistema operativo (ubuntu) é a imagem, que corresppnde à box.<br>
* O **-i -t** indica-nos a criação de uma sessão interativa. <br>
* O container é o **my-ubuntu-container**, no qual quero correr a imagem ubuntu que, por sua vez, está a correr a shell do Linux
/bin/bash. <br> <br>
* Depois de correr no terminal, é obtida a seguinte referência **root@262ed438066f**, indicando que está estou dentro do ubuntu e da bash 

3. Abrindo outro terminal referente ao meu diretório - CA4-part1 - e utilizando o comando **docker ps** é possível ver os contentores em execução,
mostrando que existe um identificador -**262ed438066f**-, correspondente à imagem do ubuntu que está a correr o comando **bash**. <br>
* Para parar o contentor: **docker stop 262ed438066f** <br>
* Para eliminar o contentor: **docker rm 262ed438066f** <br> <br>
>**NOTA**: Utilizando o comando **docker ps**, verifica-se
que o contentor foi eliminado. Para correr o contentor novamente, é possível usar o Docker instalado na nossa máquina para o
fazer mais rapidamente. No entanto, sempre que se elimina e cria um contentor, é mudada a referência.<br> <br>

4. Voltando ao primeiro terminal, usando o comando **docker images**, constato que existe outra referência -**d2e4e1f51132**. Neste caso, a referência diz
respeito à box. <br> <br>

>NOTA: Outros estados que podem ser atribuídos aos containers + ID: 
1.docker pause <br>
2.docker unpause <br>
3.docker start <br>
4.docker restart <br>
5.docker stop <br>
6.docker run<br>
7.docker kill<br> <br> 


<h3> EXERCÍCIO PRÁTICO </h3>
1. Criação do documento CA4-partA/Dockerfile <br> 

2.Criação da imagem <br>

        docker build -t 1211748 .

![img.png](Imagens/dockerbuild.png) <br> 

3. Confirmar as imagens/box criadas <br>
**docker images** <br> <br> 

4. No terminal referente à pasta CA4-partA, colocar a imagem a correr **1211748**. Para verificar a existência das
imagens e containers, colocar o comando **docker ps -a** : <br>

   
    docker run -p 59001:59001 -d 1211748 
    docker ps -a 

5. Abrindo a pasta principal - **CA4-part1** -, simula-se o chat através dos seguintes comandos:


    ./gradlew build 

    ./gradlew runClient 

![](Imagens/chat.png)   

6. Para enviar para o Docker Hub: 

docker login
![](Imagens/dockerloginA.png)  

docker tag (Deve ser colocado o ID da imagem: "37b79eb8f0af")
![](Imagens/dockertagA.png)  

>Nota: Entretanto surgiu a necessidade de apagar tags já criadas:
![](Imagens/deletetag.png)

docker push
![](Imagens/dockerpush.png)


____ PART B ____ 

1. git clone https://anamtguedes@bitbucket.org/luisnogueira/gradle_basic_demo.git  
   ![](Imagens/clonepartB.png)


2. Depois, colar o ficheiro **basic_demo-0.1.0.jar** dentro da pasta **CA4-partB**
   ![](Imagens/verificaçãodapastajar.png)

3. Correr a aplicação 










