DEVOPS

# WEEK 8 - PART 1
## Continuos integration and delivery  

<h3> JENKINS </h3>

<h4> Continuous integration </h4>

A integração contínua pode ser feita através de um repositório central de controlo de versões, como o Bitbucket.
Desta forma, sempre que há um *commit* dá-se origem à execução de todo o processo de compilação, por forma a verificar se 
o projeto está válido. <br>

<h4> Continuous delivery </h4> 
Havendo compilação e os testes correndo, terei uma versão nova do meu projeto.

<h4> Servidores de integração </h4>
O servidor mais conhecido é o Jenkins, integrando-se com ferramentas de build, como Ant
Maven e Gradle. O Jenkins tem uma imensidade de plug-ins. 

<h4> JENKINS </h4>

2. Download do jenkins e guardar na pasta **Apps** - **https://www.jenkins.io/download/** <br> <br>
3. 
4. Correr o Jenkins: **java -jar jenkins.war** <br> <br>
5. 
6. Criação de um **username** e configuração do Jenkins URL: **http://localhost:8080/** <br> <br>
7. 
![img.png](Imagens/imagem1.png)
7. Na dashboard no Jenkins, clicar em **Manage Jenkins** e **Manage Plugins**. Neste segmento,
é possível ver os plugins instalados. O segmento **Manage Credentials** é importante, na medida em que
quando o Jenkins está a correr, tem de ir ao Bitbucket buscar o código, logo é
necessário definir as credenciais quando o Jenkins for ao Bitbucket buscá-lo. <br> <br>

<h4> JENKINS PIPELINE PLUGINS </h4>

1. Existem plugins que permitem a criação de Jenkinsfile - ficheiro de programação como o Vagrantfile e Dockerfile. Este 
ficheiro utiliza a mesma linguagem utilizada pelo Gradle - **Groovy**. <br> <br>
2. A pipeline é uma sequência de stages e dentro de cada stage, são ditados os comandos. Este script é iniciado com **node** - 
no Jenkins é possível definir n computadores físicos ou virtuais, aos quais são chamados **nodos** de integração. Desta forma, é possível
descrever as caraterísticas dos nodos que têm de executar a pipeline, podendo distribuir as tarefas das pipelines por diversos
computadores. <br> <br>

<h4> JENKINS VOCABULARY </h4>

1. **Job** - tempo que o Jenkins usa para um projeto <br> <br>
2. **Node** - agente que vai executar a pipeline <br> <br>
3. **Stage** - conceito teórico de organização <br> <br>
4. **Step** - significado concreto dentro dos stages <br> <br>


<h4> PIPELINE STAGES - EXEMPLO 1 </h4>

1. Abrir o script criado pelo Professor Alexandre e copiá-lo - **https://gist.github.com/atb/2a5587bd5392dfada81a2f74e811824b** <br> <br>
2. Abrir o Jenkings -- New Item -- Criar item (não esquecendo de clicar em pipeline) <br> <br>
![img.png](Imagens/imagem2.png) <br> <br>
3. Colar script, garantindo que a Definition está em *Pipeline script*  e o projeto fica criado <br> <br>


    pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'anaguedes', url: 'https://bitbucket.org/anamtguedes/devops-21-22-lmn-1212748/'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                dir ('CA5-part1/gradle_basic_demo/') {
                sh './gradlew clean build'
            }
        }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ('CA5-part1/gradle_basic_demo/') {
                    
                archiveArtifacts 'build/distributions/*'
                    }
                
                }
            }
        }
    }


![img.png](Imagens/imagem3.png) <br> <br>
4. Dentro da dashboard, clicar no segmento **Build now** <br> <br>
![img.png](Imagens/imagem4.png)

<h4> PIPELINE STAGES - EXEMPLO 2 </h4>

* Stage Checkout - comando git para ir buscar a aplicação <br>
* Stage Build - fazer o build da apresentação, evocando o **./gradlew clean build** <br>
* Archiving - vai guardar no jenkins o ficheiro que .jar ou .war compilado <br>

    
        pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'anaguedes', url: 'https://bitbucket.org/anamtguedes/devops-21-22-lmn-1212748/'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                dir ('CA5-part1/gradle_basic_demo/') {
                sh './gradlew clean build'
            }
        }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ('CA5-part1/gradle_basic_demo/') {
                    
                archiveArtifacts 'build/distributions/*'
                    }
                
                }
            }
        }
    }

<h4> CRIAÇÃO DO DOCUMENTO JENKINSFILE </h4>

* 1 - Copiar o repositório do Professor Luís Nogueira para o nosso repositório, através da opção
**Fork Repository** (esta opção encontra-se no canto superior direito do repositório original). <br> <br> 
* 

* 2 - Criar um documento **Jenkinsfile** na pasta **gradle_basic_demo-anaguedes** e copiar o código do exemplo 1 para o ficheiro. <br> <br>

* 3 - Depois de utilizar os comandos **git**, volto ao Jenkins e altero a Description para **Pipeline script from SCM** e o Source Controll
Management (SCM) para git. Posteriormente introduz-se na URL o clone do repositório disponibilizado pelo Professor Luís Nogueira. <br> <br>

* 4 - Criação das credenciais: Manage Jenkins, Manage Credentials. <br>

> Ao contrário do script correspondente ao exemplo número 1, onde o ID é igual ao nome criado para entrar no Jenkings, quando se procede à criação de 
credenciais pode ser susbtituído por **git credentialsId: 'anaguedes-bitbucket-ssh-credentials'**.

* 4 - Fazer build novamente 

<h3> EXERCÍCIO PRÁTICO </h3>

1. Criação de 2 stages: Assemble e Test

>Note-se que o stage **Checkout** foi também adicionado, mas copiado dos restantes exemplos. Foi somente feita uma 
pequena alteração no caminho. O mesmos ocorreu com o stage **Archiving**.

    pipeline {
    agent any

    stages {
        stage('Checkout') { --> Nome do stage 
            steps {
                echo 'Checking out...'
                git credentialsId: 'anaguedes-bitbucket-ssh-credentials', url: 'https://bitbucket.org/anamtguedes/devops-21-22-lmn-1212748/' --> indica as credenciais para aceder ao bitbucket e o respetivo repositório
            }
        }

        stage('Assemble') {
            steps {
                echo 'Assembling...' 
                dir ('CA5-part1/gradle_basic_demo/') {
                    sh './gradlew clean assemble'
                }
            }
        }

        stage('Test') {
            steps {
                echo 'Testing...'
                dir ('CA5-part1/gradle_basic_demo/') {
                    sh './gradlew test'
                }
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ('CA5-part1/gradle_basic_demo/') {
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }                      
    }

    post { --> ocorrre após a pipeline 
        always { --> corre os passos dentro do post mesmo que a pipeline não tenha sido terminada 
            junit 'CA5-part1/gradle_basic_demo/build/test-results/test/*.xml'
        }
    }
}


![img.png](Imagens/imagem71.png)

Build feito com sucesso! <br> <br>

>Desta vez, a introdução do script foi feita através do Jenkinsfile, pelo que na parte da Definition de um novo item, foi necessário alterar o seguinte: <br>
* Pipeline script from SCM <br>
* SCM <br>
* **Repository URL** - https://bitbucket.org/anamtguedes/devops-21-22-lmn-1212748/ <br>
* **Credentials** - anamtguedes <br>
* **Script path** - CA5-part1/gradle_basic_demo/Jenkinsfile <br>

