DEVOPS

# WEEK 9 - PART 2
## Continuos integration and delivery 

>Nota: O conteúdo teórico abaixo, lecionado na aula teórica correspondente, não será aplicado neste exercício. No entanto, será útil para
o desenvolvimento do projeto, pelo que aprendizagem ficou na mesma registada neste ficheiro.

<h3> ANSIBLE </h3>

Gere todos os recursos necessários para o *environment* de uma aplicação correr, como configurações de software e hardware.
Outro conceito é a *infrastructure* que corresponde ao *environment* de todas as aplicações. <br> <br>

Não é preciso nenhum software especial nos computadores que vão ser controlados pelo Ansible. <br> 
 
O Vagrant como o Docker permitem configurar máquinas virtuais, enquanto o Ansible permite configurar máquinas físicas
ou virtuais. <br>

<h3> Princípios do Ansible: </h3>

* 1 - Declarar o estado da infraestrutura (Exemplo: Para o computador A quero a versão 10, para o B quero a versão 11) <br>
* 2 - A infraestrutura deve ser autonómica, ou seja, deve corrigir-se automaticamente <br> 
* 3 - Saber o estado da infraestrutura <br>

<h3> Conceitos do Ansible: </h3>

* 1 - Playbooks - Tarefas para executar em determinado computador. <br>
* 2 - Modules - Plugins para executar as tarefas, permitindo ao Ansible fazer as suas instalações. <br>

<h3> Correr as 3 box: </h3>

* 1 - Clone do repositório: **https://github.com/atb/vagrant-with-ansible**
* 2 - **vagrant up**
* 3 - **vagrant ssh ansible** (usar este comando para entrar dentro das box)
* 4 - **ansible host1 -m ping** (ver o que devolve a máquina host1 no módulo ping)
>Neste exercício, não foi necessário explorar o ansible. No entanto, tentei experimentar sem sucesso.  
Deixo no entanto, provisioriamente, o resultado do comando suprarreferido no ponto 4, o qual pretendo corrgir.

![img.png](Imagens/imagem8.png)

<h4> Exercício prático </h4>

* 1 - Instalação do plugin **HTML publisher** para publicação do JAVADOC (Jenkinds --> Manage Jenkins --> Manage Plugins --> HTML Publisher) <br>
* 2 - Instalação do plugin **Docker Pipeline** para facilitar a compilação do Docker <br>

>Nota: No fim da instalação dos plugins é necessário reiniciar o Jenkins 
 
* 3 - Para publicar no Dockerhub, usar o ID criado no **CA5-part1**: **anaguedes** <br>

Criação Dockerfile:

    FROM ubuntu:18.04

    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y

    COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .

    EXPOSE 59001

    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

* 4 - Criação Jenkinsfile: 

pipeline {
agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'anaguedes-bitbucket-ssh-credentials', url: 'https://bitbucket.org/anamtguedes/devops-21-22-lmn-1212748/'
            }
        }


        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ('CA5-part2/gradle_basic_demo/') {
                    sh './gradlew clean assemble'
                }
            }
        }

        stage('Test') {
            steps {
                echo 'Testing...'
                dir ('CA5-part2/gradle_basic_demo/') {
                    sh './gradlew test'
                }
            }
        }

        stage('Generate Jar') {
            steps {
                echo 'Generating jar...'
                dir ('CA5-part2/gradle_basic_demo/') {
                    sh './gradlew jar'
                }
            }
        }

        stage('Javadoc') {
            steps {
              echo 'Creating javadoc...'
                 dir ('CA5-part2/gradle_basic_demo/') {
                    sh './gradlew javadoc'
                        }
                    }
                }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ('CA5-part2/gradle_basic_demo/') {
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }

        stage('Docker Image') {
          steps {
            echo 'Creating an image...'
               dir ('CA5-part2/') {
                    script {
                    docker.withRegistry ('https://registry.hub.docker.com', 'anaguedes')
                    def image = docker.build("anaguedes/ca5-part2:${env.BUILD_ID}")
                    image.push ()

                        }
                    }
                }
           }
       }

    post {
                always {
                    dir('CA5-part2/gradle_basic_demo') {
                    junit 'build/test-results/test/*.xml'
                    }
                    publishHTML([allowMissing: false,
                                alwaysLinkToLastBuild: false,
                                keepAll: false, reportDir: 'CA5-part2/gradle_basic_demo/build/docs/javadoc',
                                reportFiles: 'index.html',
                                reportName: 'HTML Report',
                                reportTitles: 'The Report'])
                                }
                    }
    }

![img.png](Imagens/imagem9.png)

Infelizmente, o stage "Docker Image" não está a funcionar. Tentei solucionar o problema, mas sem sucesso. 

<h3> Alternativa Jenkins </h3>

**CircleCI** 

Circle CI é uma ferramenta de integração contínua. A ferramenta permite que sejam criados testes automatizados a 
cada tentativa de atualização na branch master. <br> <br>

O Circle CI foca-se na testagem de todas as alterações antes de serem implementadas, sendo possível 
construir todos os *jobs* através de um ficheiro **circle.yaml**







