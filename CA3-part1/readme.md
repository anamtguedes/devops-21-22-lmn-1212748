DEVOPS

# WEEK 4
## Virtualization with Vagrant 

A Virtual Box (VB) permite caraterizar uma máquina virtual, como a definição do sistema operativo e colocar o Spring Boot a correr dentro da VB. <br> <br>

<h3> CONFIGURAÇÕES DA VIRTUAL BOX </h3> 
1.	Após a instalação da Virtual Box é possível definir as caraterísticas da máquina virtual, começando por definir o nome da mesma, definir o seu local de armazenamento na máquina física, o tipo de sistema operativo  (Linux) e a versão (Ubuntu 64-bit). <br> <br>

2.	Posteriormente, são perguntadas as caraterísticas do hardware virtual, tendo o professor sugerido 1GB ou 2GB de RAM. Optei por 1GB. <br> <br>

3.	Sendo esperado que o computador precisa de um disco, existem três hipóteses a considerar: 1) Não adicionar o disco virtual; 2) Criar um disco virtual, utilizando o tamanho máximo recomendado 10GB; 3) Usar um disco virtual já existente. Optei pela opção 2. <br> <br>

4.	Seguidamente, optou-se pelo tipo de disco – VirtualBox Disk Image. <br> <br>

5.	Existem dois tipos de alocação - Alocação dinâmica (A VB irá usar, no máximo,10GB do disco criado) ou Tamanho Fixo (A VB irá destinar 10GB para o disco criado independentemente se são usadas todas as GB) -, tendo sido escolhida Alocação dinâmica. <br> <br>

6.	Criação da VB concluída: <br> <br>
a.	1GB de RAM  <br> 
b.	Controller SATA – disco de 10GB criado <br> <br>

7.	Neste momento, o computador está “vazio”, como se tivesse saído da fábrica. Quando o iniciamos, a VB alerta para a introdução de um CD-ROM virtual para instalar o sistema operativo. Tal como na aula teórica, não foi introduzido nada, mostrando apenas o hardware quando arranca. Clicando no 5º ícone a contar da esquerda é possível redimensionar a imagem. Note-se que o rato fica cingido ao hardware, sendo necessário clicar no “command” para ter liberdade de movimento na máquina física. Para desligar a máquina, clica-se em “Power Off”. <br> <br>

8.	Para instalar o CD-ROM, acedeu-se ao seguinte site - **https://help.ubuntu.com/community/Installation/MinimalCD** -, instalando-se o Ubuntu 18.04 “Bionic Beaver” 64MB. O ficheiro descarregado denomina-se **mini.iso**. Este ficheiro vai ser adicionado às configurações, na parte Storage, onde está escrito Empty como indicativo da não existência de CD-ROM. O CD-ROM do mini.iso é então inserido na VB, sendo necessário ainda clicar em “Live CD”. A partir deste momento, o computador irá ler este CD. <br> <br>

9.	Depois da seleção em “Live CD”, é agora possível instalar o Ubuntu. Durante a instalação é perguntado de que forma é instalado o disco duro, tendo-se escolhido o disco de 10GB iniciais. Esta versão não tem desktop gráfico. <br> <br>

10.	Depois de tudo instalado, retira-se o CD-ROM. Para tal, clica-se em Devices – Optical Drives – Retirar a seleção do **mini-iso** através de **remove disk from virtual drive**. <br> <br>

**NOTA1**: Para o Ubuntu correr, é necessário abrir novamente a máquina virtual e introduzir o username e a password. <br> <br>

<h3> CRIAÇÃO DE UM NOVO ADAPTADOR </h3> 
Para poder ligar a máquina virtual ao host é necessário aceder a um novo adaptador. <br> <br>

Clicando em Network, só existe um adaptador – NAT. É necessário criar um segundo adaptador para poder ligar as máquinas virtuais ao host. O NAT apenas liga à internet, mas não é possível aceder a outros packages virtuais ou ao meu host. Clicando em Host Network Manager é possível verificar as redes virtuais listadas através do IP. No meu caso surge o seguinte IP: 192.168.56.1/24. Os primeiros três octetos identificam a rede e o segundo octeto identifica o computador. <br> <br>

Posteriormente, é possível identificar o segundo adaptador com Host-only Adapter e o respetivo nome da rede (vboxnet0). O adaptador fica então ligado a uma rede nova que comunica entre a máquina virtual e o host. <br> <br>

Vamos usar uma segunda placa de rede para usar entre a máquina virtual e a máquina física. Vamos usar o Host-only Adapter (interface de rede). 

<h3> FERRAMENTAS PARA TRABALHAR COM A MÁQUINA VIRTUAL </h3> 
12.	Foram aplicadas algumas ferramentas para trabalhar com a máquina virtual. Note-se que o comando **sudo**, que será a primeira palavra a ser introduzida no output, permite ao administrador delegar a execução de tarefas. <br> <br>

a.	Atualização de todos os softwares no Linux: **sudo apt update **, em que **apt** é uma ferramenta que permite a instalação de pacotes. <br> <br>

b.	Configurar a rede: **sudo apt install net-tools**, em que **net-tools** permite fazer a gestão da rede, possibilitando o comando que surge no ponto seguinte. <br> <br>

c.	Editar um ficheiro que permite configurar os endereços das placas de rede: sudo nano /etc/netplan/01-netcfg.yaml, em que **etc** indica ficheiros de configuração e **netplan** indica os ficheiros de configuração da rede. <br> <br>

d.	Posteriormente, é aberto um editor de texto, tendo já a configuração para o adaptador 1 (enpOs3). Nesta fase, foi então necessário configurar o adaptador 2 (segunda placa de rede), indicando o endereço configurado na rede. Comandos utilizados: <br> <br>
enp0s8: <br> 
addresses: <br>
- 192.168.56.5/24 (significa que a segunda interface terá este endereço físico)
É importante referir que o texto tem de estar alinhado com o restante. Caso contrário, não é possível avançar, uma vez que os ficheiros .yaml têm uma linguagem muito específica. <br> <br>

e.	Aplicar as novas alterações: **sudo netplan apply** <br>
f.	Entrar remotamente na placa virtual através do comando: **sudo apt install openssh-server** <br>
g.	Configuração para poder entrar remotamente através da password: 
Para tal, é necessário retirar o # da linha **PasswordAuthentication yes**, mudando de azul para branco. Isto significa que a password fica ativa, gravando-a com Control+O e Control+X para sair. <br>

h.	Ler as alterações: **sudo service ssh restart**. <br>
i.	Software que permite copiar ficheiros: **sudo apt install vsftpd**, em **ftpd** significa **file transfer protocol**. <br>
j.	Editar o ficheiro de configuração para permitir a escrita: **sudo nano /etc/vsftpd.conf**, procurando-se a linha que diz **write_enable=YES** para retirar do modo comentário (eliminação do #). Volta-se a gravar com Control+O e Control+X para sair. <br>
k.	Reler configuração: **sudo service vsftpd restart**. <br>

**PONTO DA SITUAÇÃO**: Configuração da rede, instalação de acesso remoto, instalação de software para copiar ficheiros remotamente.

<h3> UTILIZANDO O TERMINAL...</h3> 

Abrir terminal no computador e colocar o comando **ssh anaguedes@192.168.56.5**, possibilitando aceder ao computador virtual. Para tal, a máquina virtual tem de estar sempre ligada, caso contrário não é possível aceder através do terminal. <br> <br>

Descarregar FileZilla para efetuar a cópia remota, sendo possível arrastar ficheiros da máquina física para a virtual e vice-versa.<br> <br>

No terminal e dentro do Linux, foram instalados os pré-requisitos para instalar a aplicação de Spring, utilizando-se os comandos: **sudo apt install git** (instalação do git) e **sudo apt install openjdk-8-jdk-headless** (instalação do java). <br> <br>

Clonar o tutorial Spring - ** git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git ** - para a máquina virtual, na qual está instalado o Linux. Este processo foi feito a partir do terminal. Dentro da pasta basic (chega-se até ela através do comando **cd**). Posteriormente utiliza-se o comando ** ./mvnw spring-boot:run**, onde são descarregadas as dependências e é corrida a aplicação. Correr a aplicação significa que o Sprint Boot cria um servidor web e fica estagnado, esperando que os clientes se liguem através do browser. O browser é aberto no computador físico - ** http://192.168.56.5:8080/**, mas vez de colocar **localhost**, coloca-se o IP criado na máquina virtual. <br> <br>

**NOTA2**: A necessidade deste comando surge porque foi criada uma nova placa de rede que permite fazer a comunicação entre máquinas virtuais e o host 


<h3>UTILIZANDO O MEU REPOSITÓRIO</h3>

Em primeiro lugar, foi necessário repor a palavra-passe depois de clonar o meu projeto: **git clone https://anamtguedes@bitbucket.org/switch-2021/projectg2.git**. Sempre que a máquina virtual é fechada, é necessário clonar o projeto e referir a password. A partir daí, é possível trabalhar nas pastas do projeto, buscando-as através dos comandos **ls**. Escolhendo -se a pasta **devops-21-22-lmn-1212748**, encontra-se o exercício pretendido. <br> <br> 

O exercício pedia que fosse retomada a experiência da simulação de chat. Para tal procedeu-se à execução do servidor dentro da VB e à execução dos clientes na minha máquina física. <br> <br>

<h3> ABERTURA WEB (EXERCÍCIO CA1) </h3>
Os procedimentos utilizados foram os referidos pelo Professor, com a diferença que foi utilizado o meu repositório. 

<h3> ABERTURA DE DOIS TERMINAIS PARA SIMULAÇÃO DE CHATS (EXERCÍCIO CA2-PART1) </h3>
 
Primeiro terminal – Este terminal está conectado à máquina virtual, podendo aceder-se à pasta **gradle_basic_demo**, contida na pasta **CA2-part1**. Neste terminal foi escrito o seguinte comando, tendo em conta a interação com o utilizador: ** .gradlew spring-boot:run**. 

Segundo terminal – Este terminal está conectado à minha máquina física, tendo sido necessária uma alteração no corpo da task **runClient**: mudou-se de ‘localhost’ para o IP da máquina virtual: **192.168.56.5**. Esta mudança é importante na medida em que agora estamos a comunicar com um computador diferente.

**Precalços**: A task **myZip** estava a dar conflito, sendo necessário comentá-la para continuar com o procedimento. Através do terminal, utiliza-se o comando **nano build.gradle** e comentar a task em questão. 
