DEVOPS

# WEEK 3
## CA2, Part1 - Unit Tests 



1. Fazer clone do projeto para a pasta CA2-part1. Esta pasta foi criada através do comando **mkdir** <br> <br>
2. Em primeiro lugar, é necessário desvincular o **.git** do diretório **gradle_basic_demo**.
ao repositório através do terminal. Para tal, usa-se o output **rm -rf .git**. Posteriormente,
é possível verificar que foi retirado através dos comandos **ls -a**.<br> <br>
3. Depois de prosseguir os passos dentro da documentação README.md disponibilizado pelo Professor 
foi possível criar um chat que simula a comunicação entre o servidor e o cliente. <br> <br>
>Note-se que o gradle apenas serve para armazenar tarefas. Para executar tasks, sem instalar o gradle no CPU,
> utiliza-se o comando ./gradlew, que tem como função “desembrulhar” a versão utilizada pelo criador do projeto.
4. Para que o servidor seja executado automaticamente sem que, para isso, seja necessário o seguinte 
output - **java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>**- 
foi adicionada a task runServer na classe build.gradle para otimizar esse processo. 
Posteriormente, na linha de comandos apenas evoquei o output **./gradlew runServer**. <br> <br>
5. Para adicionar o junit 4.12 adicionou-se no ficheiro build.gradle – dependencies, o seguinte
output: **testImplementation ‘junit: junit: 4.12’**
6. Para realizar a task COPY e ZIP foi encontrada a solução online. <br> 

<h3> COPY</h3>

>task copyDocs (type:Copy) { <br>
from ('src') <br>
into ('copy/target/doc') <br>
}

<h3> ZIP </h3>

>task myZip (type:Zip) { <br>
from('src') <br>
archieveName 'Zip_test.zip' <br>
destinationDir(file('build')) <br>
} 

7. Por último, foi adicionada a tag **ca2-part1** com o nome "refactor'.
Para tal foram usados os seguintes comandos: <br> <br>
**git tag -a ca2-part1 -m 'refactor'** <br> <br>
**git push origin ca2-part1**

> Como não me lembrava do nome associado ao commit, utilizei 
o comando **git log**