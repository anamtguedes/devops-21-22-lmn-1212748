DEVOPS

# WEEK 4 <br>
## CA2, PART2 - FROM MAVEN TO GRADLE 


 <h3> MAVEN OU GRADLE? </h3>
 O gradle é uma ferramenta para o processo build do projeto. Entende-se por **build** todas as etapas de verificação de dependência externas (bibliotecas), testes unitários, integração, compilação e empacotamento (ex: .jar ou .war). Todo este processo de automatização depende de ferramentas como o Maven e o Gradle. <br>

 Maven: configurações num arquivo pom.xml <br> <br>
 Gradle: Tal como o Maven permite gerenciar dependências e automatizar processos build, mas permite que sejam utilizados do Maven e repositórios particulares. O arquivo **build-gradle** permite uma programação das configurações. <br> <br>

<h3> CRIAÇÃO DO NOVO BRANCH </h3>
1. Foi criado um branch novo designado **tut-basic-gradle** através dos seguintes outputs : <br>

**git branch tut-basic-gradle** <br> <br>
**git commit -a -m 'added a new branch'** <br> <br>
**git push origin tut-basic-gradle** <br> <br>

 Depois de estar criado o branch, foi criada uma pasta CA2-part2 dentro do branch por questões de organização. No entanto, verifiquei que os outputs suprarreferidos, o **branch tut-basic-gradle** não aparecia no repositório remoto. <br> <br>

<h3> POR QUE RAZÃO O BRANCH CRIADO NÃO SURGIA NO REPOSITÓRIO LOCAL? </h3>

2.Para que o branch apareça no repositório remoto foi necessário utilizar o comando **--set-upstream**, equivalente a **-u**. Desta forma, o ouput **git push --set-upstream origin tut-basic-gradle** indica que queremos que o branch local esteja igualmente no repositório. <br> <br>

De acordo com o enunciado, foi gerado um ficheiro com novas dependências através do link **https://start.spring.io/**, tendo por objetivo iniciar um novo projeto em gradle spring boot, preenchendo-se os campos de acordo com as indicações dadas. A versão usada de Spring Boot foi a **2.6.6**. <br> <br>

>O Spring Boot é uma ferramenta usada para facilitar as configurações de um projeto para a web. <br> <br>

3. De acordo com o exercício, e copiando e eliminando todos os ficheiros referidos, através do terminal introduz-se o output **./gradlew bootRun** para executar a aplicação. Verifica-se o funcionamento da aplicação através da informação declarada no terminal **Started ReactAndSpringDataRestApplication in 3.045 seconds (JVM running for 3.354)**. Posteriormente, abre-se o localhost:8080 e verifica-se que está vazio. Estas tarefas correspondem aos pontos 3,4,5 e 6 do documento **CA2, Part2**. 

4. Para interagir com o código frontend, foi instalado o plugin **org.siouan.frontend**. 


<h3> COMO INSTALAR E CONFIGURAR O PLUG-IN? </h3>
A sua instalação depende da versão usada do java. Neste caso, versão 11.  Tal como indicado, cola-se o output **id "org.siouan.frontend-jdk11" version "6.0.0"**  no ficheiro build.gradle na secção plugins. <br> <br>

Para configurar o plug-in, adiciona-se o código no ficheiro **build.gradle** <br> <br>
frontend { <br> 
nodeVersion = "14.17.3" Nota: Framework para trabalhar com javascript <br> 
assembleScript = "run build" <br> 
cleanScript = "run clean" <br> 
checkScript = "run check" <br>
}<br> <br>

Posteriormente, na pasta **package.json** substitui-se o scripst pelo seguinte: <br> <br>
"scripts": { <br>
 "webpack": "webpack", <br>
 "build": "npm run webpack", <br>
 "check": "echo Checking frontend", <br>
"clean": "echo Cleaning frontend",  <br>
"lint": "echo Linting frontend", <br>
"test": "echo Testing frontend" <br>
}, <br>

Estes scripts informam o plug-in conectado ao frontend como irá realizar as suas tarefas. Por exemplo, o build dá instrução de execução do webpack - ferramenta que vai usar o código javascript da aplicação (js) - coloca-o dentro no ficheiro build (pasta apagada anteriormente e construída pelo webpack). 

>Nesta fase, temos já a configuração frontend, pelo que já é possível executar na linha de comandos o output **./gradlew build**. Se alterasse código java, esta operação teria surtido efeito. Como mudei apenas ficheiros de configuração o output referido não teve impacto. 

>NOTA: Sempre que há instalação de plugins é preciso fazer **./gradlew clean** e só depois **/gradlew build**, seguido de **./gradlew bootRun**. 

<h3> APLICAÇÃO DE CÓDIGO </h3> 

<h4> EXERCÍCIO 1 </h4>
De acordo com o ponto 13 - **Add a task to gradle to copy the generated jar to a folder named "dist" located a the project root folder level** -, foi aplicado o seguinte código: <br> 

task copyJar (type:Copy, dependsOn: jar) {<br>
   group = "CA2-part2" <br>
   from ('build/libs/react-and-sprint-data-rest-basic-0.0.1-SNAPSHOT.jar') <br>
   into ('dist') <br>
}

Neste contexto, **dependsOn** informa que a task **copyJar** depende de jar. <br> <br>

Se eu fizer **./gradlew tasks**,  mostra-me as tasks associadas a um grupo, que deve ser descrito na função como “group”. Para que todas as taks apareçam, necessito de escrever  **./gradlew tasks - - all**. No entanto, se quiser filtrar por grupo, dentro dessa função acrescento, por exemplo, “group: CA2-part2”. <br> <br>

<h4> EXERCÍCIO 2 </h4>
De acordo com o ponto 14, o utilizador teria de eliminar todos os ficheiros gerados pela webpack antes da task clean. Desta forma, foi implementado o seguinte código: <br>

**SOLUÇÃO 1** <br> 

task deleteWebpack(type: Delete) { <br>
   delete ('src/resources/main/static/built/') <br>
} <br> <br>

clean.dependsOn deleteWebpack <br>

**SOLUÇÃO 2** <br> 

clean.doFirst {
   delete ('src/resources/main/static/built/')
} <br> <br>

>Para correr a task, escreve-se na linha de comandos: <br> 

**./gradlew deleteWebpack**

<h3> IMPREVISTOS </h3> 

O ficheiro  **.DS_Store** estava a entrar em conflito relativamente à mudança de branch, pelo que foi necessário movê-lo para a pasta **.gitignore**. Como a tinha eliminado no repositório remoto, voltei a criá-la. Como? <br> 

Utilização do comando **touch .gitignore** e respetivos add, commit e push. 
Para mover o ficheiro .DS_Store para o .gitignore utilizei o seguinte comando: <br> 
**git update-index --assume.unchanged .DS_Store**

Foi assim possível fazer a mudança para o branch master através do output **git checkout master** e transferir o conteúdo contido no branch tut-basic-gradle, nomeadamente a pasta ca2-part2 para o branch master. 

<h3> TAGS </h3> 

Para finalizar, o último commit associado ao branch master recebeu a seguinte tag **ca2-part2**, de acordo com os seguintes ouputs: <br>:
1. git tag ca2-part2 -m 'final' <br>
2. git push origin ca2-part2 <br> <br>

NOTA: Tendo em conta que foi primeiramente criada a pasta CA2-part2 no branch **tut-basic-gradle**, o envio da informação para o master exigiu que os add, commit e push fossem sempre em duplicado para a informação ficar igual. Foi somente necessário ter em atenção a mudança de branch. 